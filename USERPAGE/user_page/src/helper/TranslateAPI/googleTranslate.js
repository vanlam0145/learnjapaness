const apikey = require('../../key')
const googleTranslate = require('google-translate')(apikey.default, { concurrentLimit: 20 });
export default googleTranslate;